import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt


# matplotlib inline
sns.set_style('white',{'font.sans-serif':['simhei','Arial']})

#导入seaborn自带iris数据集
data=sns.load_dataset("iris")  
#为了方便大家观看，把列名换成中文的
data.rename(columns={"sepal_length":"A",
                     "sepal_width":"B",
                     "petal_length":"C",
                     "petal_width":"D",
                     "species":"E"},inplace=True)
kind_dict = {
    "setosa":"山鸢尾",
    "versicolor":"杂色鸢尾",
    "virginica":"维吉尼亚鸢尾"
}
data["E"] = data["E"].map(kind_dict)
data.head() #数据集的内容如下 

sns.pairplot(data)


plt.show()